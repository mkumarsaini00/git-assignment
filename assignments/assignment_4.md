# Assignment #4

Learn git from: https://git-scm.com/docs/gittutorial

Before beginning with the assignment make sure you registered on http://gitlab.com/

If you have not registered yet, please create an account on http://gitlab.com/


1. Fork this repository on gitlab account: https://gitlab.com/symb-assessment/startbootstrap-bare
2. Add tag to the repository as version *v0.0.1*
3. Create new branch 'content_changes' remove all the content from the home page and write 
    ```html
    <h1>Hello World</h1>
    ```
4. Commit the edited code
5. Create new branch 'action' from master and checkout it.
6. Add new file 'action.html' with  same content as 'index.html'
7. Commit the changes
8. Create new branch 'homepage' from master and checkout it.
9. Change content again for 'index.html' as
    ```html
    <h1>Welcome to SYMB Technologies</h1>
    ```
8. Commit the changes
9. Create merge request to merge 'homepage' and approve it. Tag it as version 'v0.0.2'
10. Now merge the branch 'content_changes' to 'master'. Tag it as version 'v0.0.3'
11. Merge 'action' to 'master' and tag it as 'v0.0.4'
12. Push all the changes to the remote repository

